from django.test import TestCase
from smartcity.models import Services
from smartcity.views import *

class DatabaseTestCase(TestCase):
    def setUp(self):
        Services.objects.create(name="John", address="12 Test St", lat=17.1234, lng=10.4321, type="house", email="123@email.com", phone_number="12345", image="123.com")
        Services.objects.create(name="Sarah", address="3 Fake St", lat=10, lng=8.1, type="shed", email="321@email.com", phone_number="54321", image="4321.com")

    def test_create_database_entry(self):
        table_entry_1 = Services.objects.get(id=1)
        table_entry_2 = Services.objects.get(id=2)
        self.assertEqual(table_entry_1.type, "house")
        self.assertEqual(table_entry_1.name, "John")
        self.assertEqual(table_entry_2.image, "4321.com")
        self.assertEqual(table_entry_2.name, "Sarah")
    

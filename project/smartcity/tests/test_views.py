from django.test import TestCase
from smartcity.views import *

class ViewsTestCase(TestCase):
    
    def test_homepage_renders_html(self):
        url = reverse('homepage')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'smartcity/index.html')

    def test_login_renders_html(self):
        url = reverse('login')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'smartcity/login.html')

    def test_register_renders_html(self):
        url = reverse('register')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'smartcity/register.html')

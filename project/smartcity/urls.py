from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import views

urlpatterns = [
    url(r'^index.html', views.homepage, name='homepage'),
    url(r'^service=(?P<service_id>\d+)$', views.service, name='service_id'),
    url(r'^login.html', views.login_user, name='login'),
    url(r'^logout', views.logout_user, name='logout'),
    url(r'^register.html', views.register, name='register'),
]

urlpatterns += staticfiles_urlpatterns()

from django.apps import AppConfig


class SmartcityConfig(AppConfig):
    name = 'smartcity'

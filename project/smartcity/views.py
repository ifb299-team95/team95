from django.shortcuts import *
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from smartcity.models import *
from django.http import Http404

def homepage(request):
    table_data = Services.objects.all();

    if (request.GET.get('btnSearch')):
        search_item = request.GET['txtSearch']
        table_data = Services.objects.raw("SELECT * FROM smartcity_services WHERE name LIKE '%%" + search_item + "%%'")
    elif (request.GET.get('btnAll')):
        table_data = Services.objects.raw('SELECT * FROM smartcity_services')
    elif (request.GET.get('btnStudent') or request.GET.get('Student')):
        table_data = Services.objects.raw('SELECT * FROM smartcity_services WHERE type != "industry" AND type != "Hotel"')
    elif (request.GET.get('btnTourist') or request.GET.get('Tourist')):
        table_data = Services.objects.raw('SELECT * FROM smartcity_services WHERE type = "Hotel" OR type = "Parks" OR type = "zoo" OR type = "Museum" OR type = "restaurant" OR type = "Malls"')
    elif (request.GET.get('btnBusiness') or request.GET.get('Business')):
        table_data = Services.objects.raw('SELECT * FROM smartcity_services WHERE type = "Hotel" OR type = "industry" OR type = "Parks" OR type = "zoo" OR type = "Museum" OR type = "restaurant" OR type = "Malls"')

    context = {'table_data': table_data,}
    return render(request, 'smartcity/index.html', context)

def service(request, service_id):
    service = Services.objects.get(id = service_id)
    context = {'service': service,}

    return render(request, 'smartcity/service.html', context)
    
def login_user(request):
    if (request.POST.get('btnLogin')):
        un = request.POST['txtUsername']
        pw = request.POST['txtPassword']
        user = authenticate(username=un, password=pw)

        if (user is not None):
            login(request, user)
            
            if (user.groups.filter(name='Student').exists()):
                data = 'Student'
            elif (user.groups.filter(name='Tourist').exists()):
                data = 'Tourist'
            else:
                data = 'Business'

            return redirect('index.html?' + data + '=type')
        else:
            context = {'login_message':'Failed to login'}
            return render(request, 'smartcity/login.html', context) 
        
    context = {}
    return render(request, 'smartcity/login.html', context)

def logout_user(request):
    logout(request)
    context = {}
    return redirect(homepage)
        

def register(request):
    if (request.GET.get('btnRegister')):
        gn = request.GET['txtGiven']
        sn = request.GET['txtSurname']
        un = request.GET['txtUsername']
        pw = request.GET['txtPassword']
        em = request.GET['txtEmail']
        pn = request.GET['txtPhone']
        ra = request.GET['txtAddress']
        ut = request.GET['txtSelect']

        if (gn == '' or sn == ''):
            context = {'register_error':'Please enter your given and surname'}
            return render(request, 'smartcity/register.html', context)          
        elif (un == '' or pw == ''):
            context = {'register_error':'Please enter a username and password'}
            return render(request, 'smartcity/register.html', context)  
        elif (len(pw) < 8):
            context = {'register_error':'Password must be at least 8 characters'}
            return render(request, 'smartcity/register.html', context)
        elif (len(pn) > 10):
            context = {'register_error':'Phone number cannot be longer than 10 digits'}
            return render(request, 'smartcity/register.html', context)
        
        try:
            user = User.objects.create_user(username=un, password=pw, email=em)
        except IntegrityError:
            context = {'register_error':'Username already taken'}
            return render(request, 'smartcity/register.html', context)

        user.first_name = gn
        user.last_name = sn
        user.profile.phone_number = pn
        user.profile.residential_address = ra
        g = Group.objects.get(name=ut) 
        g.user_set.add(user)
        user.save()
        logout(request)
        login(request, user)
        context = {'user': request.user,}
        return redirect('index.html?' + ut + '=type')  

    context = {}
    return render(request, 'smartcity/register.html', context)

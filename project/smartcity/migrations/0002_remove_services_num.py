# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-10-24 00:23
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smartcity', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='services',
            name='num',
        ),
    ]

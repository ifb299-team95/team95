# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-10-27 04:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('smartcity', '0008_auto_20171025_2033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='phone_number',
            field=models.BigIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='profile',
            name='residential_address',
            field=models.CharField(max_length=200),
        ),
    ]

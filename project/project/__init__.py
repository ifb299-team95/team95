import unittest

def suite():   
    return unittest.TestLoader().discover("smartcity.tests", pattern="*.py")
